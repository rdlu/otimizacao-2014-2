#! /usr/bin/ruby
# Por Luis Fernando Schauren, Rodrigo Dlugokenski
# Otimizacao Combinatoria 2014-2 - Prof Luciana Buriol
# Ciencia da Computacao - Instituto de Informatica da UFRGS

$stderr.puts 'Conversor de formato para Otimizacao 2014-2'
$stderr.puts 'Por Luis Fernando Schauren / Rodrigo Dlugokenski'
$stderr.puts "Usando #{RUBY_DESCRIPTION}"

if(ARGV[0])
	#leitura por estados:
	# inicial : Linha inicial
	# custo_inicial : Custo de inicializacao da facilidade i
	# demanda : Demanda do cliente j
	# custo_unidade : Custo por unidade entregue da facilidade i
	@estado = :inicial
	$stderr.puts "Lendo arquivo #{ARGV[0]}."
	begin
		File.foreach(ARGV[0]) do |line|
			items = line.split(" ")
			case @estado
			when :inicial
				@facilidades = Array.new(items[0].to_i) { |i| i = { :capacidade => 0, :custo => 0 }  }
				@clientes = Array.new(items[1].to_i) { |i| i = { :demanda => 0, :custos => Array.new(items[0].to_i) { |j| j = 0.0 } } }
				@facilidade = 0
				@cliente = 0
				@facilidade_custo_unidade = 0
				@estado = :facilidades
			when :facilidades
				@facilidades[@facilidade][:capacidade] = items[0].to_i
				@facilidades[@facilidade][:custo] = items[1].to_f
				#puts @facilidades[@facilidade][:capacidade]
				#puts @facilidades[@facilidade][:custo]
				@facilidade += 1
				#puts @facilidade
				if(@facilidade >= @facilidades.count)
					@estado = :demanda_cliente
					$stderr.puts "Facilidades inicializadas Ultima/Total: #{@facilidade}/#{@facilidades.count}"
				end
			when :demanda_cliente
				@clientes[@cliente][:demanda] = items[0].to_i
				@facilidade_custo_unidade = 0
				@estado = :custo_unidade
			when :custo_unidade
				items.each do |item|
					@clientes[@cliente][:custos][@facilidade_custo_unidade] = item.to_f
					@facilidade_custo_unidade += 1
				end
				if(@facilidade_custo_unidade >= @facilidades.count)
					@cliente += 1
					@estado = :demanda_cliente
					$stderr.puts "Cliente lido! Ultimo/Total: #{@cliente}/#{@clientes.count}"
				end
			else
				raise "Estado nao previsto para leitura do arquivo"
			end
		end
	rescue Exception => e
		$stderr.puts "Erro ao ler arquivo: #{e.message}"
		$stderr.puts e.backtrace
	end

	$stderr.puts "Imprimindo dados glpk no stdout"

	begin
		puts "data;"
		puts "param: FACILITIES:\tf_cap\tf_cost :="
		@facilidades.each_with_index do |facilidade, index|
			puts "               F#{index}\t#{facilidade[:capacidade]}\t#{facilidade[:custo]}"
		end
		puts ";"
		puts ""
		puts "param: CLIENTS:\tc_demand :="
		@clientes.each_with_index do |cliente,index|
			puts "          C#{index}\t#{cliente[:demanda]}"
		end
		puts ";"
		puts ""
		puts "param unit_cost (tr)"
		line = "\t:"
		@facilidades.each_index do |index|
			line << "\tF#{index}"
		end
		puts line << "\t:="

		@clientes.each_with_index do |cliente,i|
			line = "\tC#{i}"
			@facilidades.each_index do |j|
				line << "\t#{cliente[:custos][j]}"
			end
			puts line
		end
		puts ";"
		puts "end;"
		puts ""
	rescue Exception => e
		$stderr.puts "Erro geral: #{e.message} \n#{e.backtrace}"
	end
else
	$stderr.puts '--- USO DO COMANDO: ruby conversor.rb NOMEDOARQUIVO.TXT --- '
	$stderr.puts 'A saida do arquivo GLPK eh feita no STDOUT use redirecionamento para salvar em arquivo'
	$stderr.puts 'Exemplo: ruby conversor.rb origem/arq.txt > destino/dados.dat'
end
