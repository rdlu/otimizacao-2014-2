# Por Luis Fernando Schauren, Rodrigo Dlugokenski
# Otimizacao Combinatoria 2014-2 - Prof Luciana Buriol
# Ciencia da Computacao - Instituto de Informatica da UFRGS
### Classe que define o estado corrente e os respectivos calculos
### Dados brutos devem ser consultados de CFLP_Data
class CflpState
	@data = nil
	#custo total apos calculos, inviavel ate que se abra facilidades
	@total_cost = 9999999999999999999999.9
	@opening_cost = 0
	@alocation_cost = 999999999999999999999999.9
	#ordem das facilidades desse estado
	@facility_order = nil
	#estado de ativacao das facilidades desse estado
	@activation_status = nil
	#demanda nao atendida de clientes :: o objetivo eh atender a demanda de todos, zerando essa array
	@unmet_demand = nil
	#matriz FxC contendo as unidades fornecidas por F a C
	@units_required = nil
	@open_factories = nil
	@open_capacity = 0
	@total_demand = 0


	#inicializa o estado e as estrturas que devem ser mantidas
	def initialize(data)
		#os dados lidos do arquivo, no objeto CFLP_Data
		@data = data
		@open_factories = Set.new
		#array que vai ser utilizada para verificar se a restricao de atender todos foi respeitada
		random_state
	end

	#aloca a demanda e abre as fabricas que forem necessarias para atende-la
	#usa o conceito de clientes "favoritos" para ser guloso e pegar os menos custosos
	def allocate_demand(guloso=true)
		@unmet_demand = @data.demandas.clone
		@units_required = Array.new(@data.capacidades.count) { |i| i = Array.new(@data.demandas.count) { |j| j = 0 } }
		unmet_demand_total = @unmet_demand.reduce(:+)
		
		#executa na ordem previa a alocacao da capacidade
		#enquanto houver capacidade ociosa desta fabrica e nao forem percorridos todos os clientes
		@facility_order.each do |facility_number|
			@data.clientes_favoritos(facility_number,guloso).each do |h|
				client_number = h[:cliente]
				if(@unused_capacities[facility_number] > 0)
					if(@unmet_demand[client_number] > 0)
						unidades_atendidas = (@unused_capacities[facility_number] > @unmet_demand[client_number]) ? @unmet_demand[client_number] : @unused_capacities[facility_number]
						@open_factories << facility_number
						unmet_demand_total -= unidades_atendidas
						@units_required[facility_number][client_number] += unidades_atendidas
						@unmet_demand[client_number] -= unidades_atendidas
						@unused_capacities[facility_number] -= unidades_atendidas
					end
				else
					#capacidade esgotada, vamos a proxima fabrica
					break
				end				
			end
		end
		true
	end

	#um estado aleatorio de fabricas, simplesmente sorteamos uma ordem de fabrica preferenciais e alocamos
	def random_state
		# exclamacao '!' no ruby significa "no mesmo espaco de memoria", nao cria uma nova array
		@facility_order = (0..@data.capacidades.count-1).to_a
		@open_factories = Set.new
		@total_demand = @data.demandas.reduce(:+)
		@total_cost = 9999999999999999999999.9
		@unmet_demand = @data.demandas.clone
		@units_required = Array.new(@data.capacidades.count) { |i| i = Array.new(@data.demandas.count) { |j| j = 0 } }
		@unused_capacities = @data.capacidades.clone
		@facility_order.shuffle!
		allocate_demand
		calculate_costs
		self
	end

	#vizinhanca: escolhe o melhor entre os 4 tipos de vizinhanca
	def neighbour_state
		#sorteia o estado e navega entre os sorteados ate achar um viavel
		case rand(4)
		when 0
			neighbour_facility_xchange(1,1,true)
		when 1
			neighbour_facility_xchange(2,1,true)
		when 2
			neighbour_facility_xchange_opened(false)
		else
			neighbour_facility_xchange_opened
		end
		self
	end

	#tipo de vizinhanca xchange (abre x, fecha y e realoca)
	def neighbour_facility_xchange(n_to_open,m_to_close,guloso)
		#escolhe aleatorios, n das fabricas abertas e m das fechadas
		candidates_to_close = @open_factories.to_a.sample(m_to_close)
		candidates_to_open = closed_factories.to_a.sample(n_to_open)
		if(candidates_to_open.count > 0 and candidates_to_close.count > 0)
			#temos candidatos
			#devolver os valores para nao atendidos
			candidates_to_close.map do |candidate_num|
				#devolvemos a demanda nao atendida para a array de unmet
				@unmet_demand.map!.with_index do |client_unmet_demand,client_index|
					client_unmet_demand += @units_required[candidate_num][client_index]
				end
				#devolvemos a capacidade para a fabrica
				@unused_capacities[candidate_num] += @units_required[candidate_num].reduce(:+)
				#zeramos as unidades que seriam entregues
				@units_required[candidate_num].map! {|u| u = 0}
				#removemos das abertas
				@open_factories.delete(candidate_num)
			end

			#pegar os valores nao atendidos e alocar para as fabricas a serem abertas
			required_demand = @unmet_demand.reduce(:+)
			proposed_capacity = candidates_to_open.map{|i|@unused_capacities[i]}.reduce(:+)
			#puts "Demanda rerequerida #{required_demand}, Capacidade proposta: #{proposed_capacity} [#{candidates_to_open}] [#{candidates_to_close}]"
			if(proposed_capacity >= required_demand)
				#temos capacidade suficiente nos candidatos a abertura
				if(reallocate_demand_2(candidates_to_open,true))
					#se realocamos com sucesso, calculamos o novo custo
					calculate_costs
				else
					puts "INV por nao realoc"
					@total_cost = Float::INFINITY
				end

			else
				#nao temos capacidade suficiente nos candidatos a abertura, inviavel
				puts "INV por nao capac"
				@total_cost = Float::INFINITY
			end

		else
			#nao temos candidatos, inviavel
			puts "INV por falta cand"
			@total_cost = Float::INFINITY
		end
		self
	end

	#realoca a demanda nao atendida em candidatos fornecidos
	#retorna true se atendeu, false se nao conseguiu
	def reallocate_demand(facilities_array,guloso)
		unmet_demand_total = @unmet_demand.reduce(:+)
		#executa na ordem previa a alocacao da capacidade
		#enquanto houver capacidade ociosa desta fabrica e nao forem percorridos todos os clientes
		facilities_array.each do |facility_number|
			@data.clientes_favoritos(facility_number,guloso).each do |h|
				client_number = h[:cliente]
				if(@unused_capacities[facility_number] > 0)
					if(@unmet_demand[client_number] > 0)
						unidades_atendidas = (@unused_capacities[facility_number] > @unmet_demand[client_number]) ? @unmet_demand[client_number] : @unused_capacities[facility_number]
						@open_factories << facility_number
						unmet_demand_total -= unidades_atendidas
						@units_required[facility_number][client_number] += unidades_atendidas
						@unmet_demand[client_number] -= unidades_atendidas
						@unused_capacities[facility_number] -= unidades_atendidas
					end
				else
					#capacidade esgotada, vamos a proxima fabrica
					break
				end				
			end
		end
		
		return (unmet_demand_total == 0)
	end

	#da a preferencia aos clientes fazerem as escolhas gulosas das fabricas
	def reallocate_demand_2(facilities_array,guloso)
		unmet_demand_total = @unmet_demand.reduce(:+)
		#executa na ordem previa a alocacao da capacidade
		#enquanto houver capacidade ociosa desta fabrica e nao forem percorridos todos os clientes
		clientes_afetados = @unmet_demand.each_index.select{|i| @unmet_demand[i] > 0}


		clientes_afetados.each do |client_number|
			facilities_ev_order = facilities_array.sort {|x,y| @data.fabrica_favorita_sort?(client_number,x,y) }
			facilities_ev_order.each do |facility_number|
				if(@unused_capacities[facility_number] > 0)
					if(@unmet_demand[client_number] > 0)
						unidades_atendidas = (@unused_capacities[facility_number] > @unmet_demand[client_number]) ? @unmet_demand[client_number] : @unused_capacities[facility_number]
						@open_factories << facility_number
						unmet_demand_total -= unidades_atendidas
						@units_required[facility_number][client_number] += unidades_atendidas
						@unmet_demand[client_number] -= unidades_atendidas
						@unused_capacities[facility_number] -= unidades_atendidas
					end
				else
					#capacidade esgotada, vamos a proxima fabrica
					break
				end				
			end
		end
		
		return (unmet_demand_total == 0)
	end

	def neighbour_facility_xchange_opened(guloso=true)
		candidates = @open_factories.to_a.sample(2)

		if(candidates.count > 0)
			#fechamos ambas para reabrir em seguida
			candidates.map do |candidate_num|
				@unmet_demand.map!.with_index do |client_unmet_demand,client_index|
					client_unmet_demand += @units_required[candidate_num][client_index]
				end
				#devolvemos a capacidade para a fabrica
				@unused_capacities[candidate_num] += @units_required[candidate_num].reduce(:+)
				#zeramos as unidades que seriam entregues
				@units_required[candidate_num].map! {|u| u = 0}
				#removemos das abertas
				@open_factories.delete(candidate_num)
			end
			if(reallocate_demand(candidates,guloso))
				calculate_costs
			else
				#puts "INV por nao realloc"
				@total_cost = Float::INFINITY
			end
		else
			#puts "INV por falta cand"
			@total_cost = Float::INFINITY
		end
		self
	end

	def total_cost
		@total_cost
	end

	def calculate_costs
		@total_cost = 0.0
		@opening_cost = 0.0
		@alocation_cost = matriz_custo_aloc.map{|u|u.reduce(:+)}.reduce(:+)
		@open_factories.to_a.sort.each do |facility_number|
			@opening_cost += @data.custos_inicializacao[facility_number]
		end
		@total_cost = @opening_cost + @alocation_cost
	end

	def facility_order
		@facility_order
	end

	def facility_status
		@open_factories.sort
	end

	def met_demand
		@units_required
	end

	def data
		@data
	end

	def open_capacity
		@open_capacity
	end

	def open_facilities
		@open_factories
	end

	def closed_facilities
		# = todas - as_abertas
		@facility_order.to_set - @open_factories
	end

	def closed_factories
		closed_facilities
	end


	def open_factories
		@open_factories
	end

	def units_required
		@units_required
	end

	def matriz_custo_aloc
		@matriz_custo = Array.new(@data.capacidades.count) { |i| i = Array.new(@data.demandas.count) { |j| j = 0 } }
		@units_required.map.with_index do |linha,fac|
			linha.map.with_index do |items,cl|
				@matriz_custo[fac][cl] = items * @data.custo_unitario[fac][cl]
			end
		end
		@matriz_custo
	end

	def print_state(verbose=false)
			puts "Custo encontrado: #{calculate_costs} (#{@opening_cost} + #{@alocation_cost})"
			puts "Facilidades que foram abertas: #{@open_factories.to_a.sort}"
			
			if(verbose)
				puts "Unidades requeridas:"
				@units_required.each_with_index do |linha,fabrica|
					puts "F#{fabrica} #{linha}"
				end

				m = matriz_custo_aloc
				
				puts "Capacidade utilizada pelas fabricas"
				@units_required.each_with_index do |linha,fabrica|
					puts "F#{fabrica} #{linha.reduce(:+)} #{100*linha.reduce(:+)/@data.capacidades[fabrica]}% #{m[fabrica].reduce(:+)}"
				end
				puts "Total Custo: #{m.map{|u|u.reduce(:+)}.reduce(:+)} "
				puts "Demanda atendida para os clientes"
				@data.demandas.each_with_index do |demanda,index|
					total = @units_required.map{|i| i[index] }.reduce(:+)
					puts "C#{index} #{total} #{100*total/demanda}%"
				end
			end
			""
	end
end
