#! /usr/bin/env ruby
# Por Luis Fernando Schauren, Rodrigo Dlugokenski
# Otimizacao Combinatoria 2014-2 - Prof Luciana Buriol
# Ciencia da Computacao - Instituto de Informatica da UFRGS

$stderr.puts 'SimulateAnnealing Otimizacao 2014-2'
$stderr.puts 'Por Luis Fernando Schauren / Rodrigo Dlugokenski'
$stderr.puts "Usando #{RUBY_DESCRIPTION}"
DEBUG=false

require './cflp_data.rb'
require './cflp_state.rb'
require './simulate_annealing.rb'
require 'optparse'
require 'set'

#Parametros padrao

# Numero de rodadas maximas
RODADAS_MAX_DEFAULT = 30000
# Tempo do relogio maximo, em segundos
TEMPO_MAX_DEFAULT = 3600
# Valor aceitavel para a solucao, eh o menor numero aceitavel
ENERGIA_MAX_DEFAULT = 0
# Alpha eh o multiplicador para a reducao de temperatura, rodada a rodada
ALPHA_DEFAULT = 0.95
# Uma peqquena chance de recomecar com um estado randomico
PROB_REINICIO_DEFAULT=0.001

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: ruby run-sa.rb [options]"
  opts.on('-a', '--arquivo NAME', 'Arquivo de entrada [OBRIGATORIO]') { |v| options[:file] = v }
  opts.on('-r', '--rodadas NUMBER', 'Numero de rodadas do SA') { |v| options[:rodadas_max] = v }
  opts.on('-o', '--objetivo NUMBER', 'Custo aceitavel para a solucao') { |v| options[:energia_aceitavel] = v }
  opts.on('-p', '--probabilidade NUMERO', 'Probabilidade de reinicio (Padrao 0.001)') { |v| options[:probabilidade_reinicio] = v }
  opts.on('-t', '--tempo SEGUNDOS', 'Tempo máximo para execução') { |v| options[:tempo_max] = v }
  opts.on("-v", "--[no-]verbose", "Run verbosely") do |v|
    options[:verbose] = v
  end

  opts.on_tail("-h", "--help", "Show this message") do
        puts opts
        exit
  end

end.parse!

if(options[:file])
	a = CflpData.new(options[:file])
	b = CflpState.new(a)
	c = SimulateAnnealing.new({
		estado_inicial: b,
		rodadas_max: options[:rodadas_max] ? options[:rodadas_max].to_i : nil,
		energia_aceitavel: options[:energia_aceitavel] ? options[:energia_aceitavel].to_f : nil,
		tempo_max: options[:tempo_max] ? options[:tempo_max].to_i : (a.demandas.count * a.capacidades.count)/2
		})
	puts "Solucao inicial com custo: #{b.total_cost}"
	puts ">>>>>>>>> SA Iniciado <<<<<<<<<"
	t1 = Time.now
	final = c.start
	t2 = Time.now
	delta = t2 - t1

	#final.print_state(options[:verbose])
	puts "Tempo total real: #{delta} secs"
else
	puts options
end
