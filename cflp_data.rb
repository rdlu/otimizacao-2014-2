# Por Luis Fernando Schauren, Rodrigo Dlugokenski
# Otimizacao Combinatoria 2014-2 - Prof Luciana Buriol
# Ciencia da Computacao - Instituto de Informatica da UFRGS
### Classe que define os dados brutos que não são necessários para serem duplicados (o state terá varias copias na memoria)
class CflpData
  #inicializa a leitura dos dados
  def initialize(file)
    #leitura por estados:
    # inicial : Linha inicial
    # custo_inicial : Custo de inicializacao da facilidade i
    # demanda : Demanda do cliente j
    # custos_alocacao : Custo por unidade entregue da facilidade i
    @estado = :inicial
    puts "Lendo arquivo #{file}."
    begin
      File.foreach(file) do |line|
        items = line.split(" ")
        case @estado
        when :inicial
          @capacidades = Array.new(items[0].to_i) { |i| i = 0  }
          @custos_inicializacao = Array.new(items[0].to_i) { |i| i = 0  }
          @demandas = Array.new(items[1].to_i) { |i| i = 0 }
          @custos_por_alocacao = Array.new(items[0].to_i) { |i| i = Array.new(items[1].to_i) { |j| j = 0.0 } }
          @facilidade = 0
          @cliente = 0
          @facilidade_custo_unidade = 0
          @estado = :facilidades
        when :facilidades
          @capacidades[@facilidade] = items[0].to_i
          @custos_inicializacao[@facilidade] = items[1].to_f
          @facilidade += 1
          if(@facilidade >= @capacidades.count)
            @estado = :demanda_cliente
          end
          #puts "Facilidades inicializadas Ultima/Total: #{@facilidade}/#{@capacidades.count}"
        when :demanda_cliente
          @demandas[@cliente] = items[0].to_i
          @facilidade_custo_unidade = 0
          @estado = :custos_alocacao
        when :custos_alocacao
          items.each do |item|
            @custos_por_alocacao[@facilidade_custo_unidade][@cliente] = item.to_f
            @facilidade_custo_unidade += 1
          end
          if(@facilidade_custo_unidade >= @capacidades.count)
            @cliente += 1
            @estado = :demanda_cliente
            #puts "Cliente lido! Ultimo/Total: #{@cliente}/#{@demandas.count}"
          end
        else
          raise "Estado nao previsto para leitura do arquivo"
        end
      end
    rescue Exception => e
      puts "Erro ao ler arquivo: #{e.message}"
      puts e.backtrace
    end
    puts "Arquivo lido com sucesso. Clientes: #{@demandas.count} Facilidades: #{@capacidades.count}"

    @demanda_total = @demandas.reduce(:+)
    @capacidade_total = @capacidades.reduce(:+)
    puts "Demanda total: #{@demanda_total} Capacidade total: #{@capacidade_total}"
    #custo beneficio: custo por unidade inteira alocada de F em C
    @custo_beneficio =  Array.new(@capacidades.count) { |i| i = Array.new(@demandas.count) { |j| j = {custo:0} } }
    @custo_beneficio_2 =  Array.new(@demandas.count) { |i| i = Array.new(@capacidades.count) { |j| j = {custo:0} } }
    @custo_unitario =  Array.new(@capacidades.count) { |i| i = Array.new(@demandas.count) { |j| j = 0.0 } }
    @custos_por_alocacao.each_with_index do |clientes,i|
      clientes.each_with_index do |custo_alocado,j|
        @custo_beneficio[i][j] = {custo: (custo_alocado > 0 ? custo_alocado / @demandas[j] : 0.0), cliente: j, demanda: @demandas[j]}
        @custo_beneficio_2[j][i] = {custo: (custo_alocado > 0 ? custo_alocado / @demandas[j] : 0.0), facilidade: i, capacidade: @capacidades[i]}
        @custo_unitario[i][j] = (custo_alocado > 0 ? custo_alocado / @demandas[j] : 0.0)
      end
    end

    return self
  end

  def demandas
    @demandas
  end

  def capacidades
    @capacidades
  end

  def custos_inicializacao
    @custos_inicializacao
  end

  def custos_por_alocacao
    @custos_por_alocacao
  end

  def custo_unitario
    @custo_unitario
  end

   def custo_beneficio
    @custo_beneficio
  end

  def custo_beneficio_2
    @custo_beneficio_2
  end

  def clientes_favoritos(fact_num,guloso)
    if(guloso)
      @custo_beneficio[fact_num].sort_by {|h| h[:custo]}
    else
      @custo_beneficio[fact_num].shuffle
    end
  end

  def fabricas_favoritas(cliente,guloso=true)
    if(guloso)
      @custo_beneficio_2[cliente].sort_by {|h| h[:custo]}
    else
      @custo_beneficio_2[cliente].shuffle
    end
  end

  def fabrica_favorita?(cliente,f1,f2)
    x = fabricas_favoritas(1,true).map {|i| i[:facilidade]}
    (x.find_index(f1) < x.find_index(f2)) ? f1 : f2
  end

  def fabrica_favorita_sort?(cliente,f1,f2)
    x = fabricas_favoritas(1,true).map {|i| i[:facilidade]}
    (x.find_index(f1) < x.find_index(f2)) ? -1 : 1
  end

  def melhor_solucao
    @best_solution
  end

  def melhor_solucao= (solucao)
    @best_solution = solucao
  end

  def demanda_total
    @demanda_total
  end

end
