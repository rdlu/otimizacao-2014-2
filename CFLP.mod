# Modelagem do Capacitated Facility Location Problem
# Por Luis Fernando Schauren, Rodrigo Dlugokenski
# Otimizacao Combinatoria 2014-2 - Prof Luciana Buriol
# Ciencia da Computacao - Instituto de Informatica da UFRGS

set FACILITIES;   # origins
set CLIENTS;   # destinations

param f_cap {FACILITIES} >= 0;   # amounts available at origins
param f_cost {FACILITIES} >= 0;   # cost of activation for a facility
param c_demand {CLIENTS} >= 0;   # amounts required at destinations

check: sum {i in FACILITIES} f_cap[i] >= sum {j in CLIENTS} c_demand[j];

param unit_cost {FACILITIES, CLIENTS} >= 0;   # Production costs per allocation
var produced {FACILITIES, CLIENTS} >= 0, <= 1;    # % of capacity units to be produced
var initialized {FACILITIES} binary;    # facility will be initilized or not

minimize Total_Cost:
   sum {i in FACILITIES, j in CLIENTS} unit_cost[i,j] * produced[i,j] + sum {i in FACILITIES} f_cost[i] * initialized[i];

subject to Demand_Percent {j in CLIENTS}:
   sum {i in FACILITIES} produced[i,j] = 1.0;

subject to Initialized_Capacity {i in FACILITIES}:
   sum {j in CLIENTS} c_demand[j]*produced[i,j] <= f_cap[i]*initialized[i];

solve;

printf "Custo total: %.5f\n",Total_Cost;
printf "Fabricas inicializadas\n";
printf {i in FACILITIES} "%s: %d\t",i,initialized[i];
printf "\n";
end;
