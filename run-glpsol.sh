#! /bin/bash
files=(41 51 61 71 81 91 101 111 121 131)

for i in "${files[@]}"
do
	file="cap$i"
	echo "!! Rodando o GLPSOL com o arquivo dat/$file.dat"
	echo "## RELATORIO GLPSOL LFSCHAUREN/RDLUGOKENSKI PARA $file.dat ##" > reports/$file.txt
	for j in {1..10}
	do
   		echo "!! Rodada $j do $file. ++ Inicio: `date`"
   		echo "!! Rodada $j do $file. ++ Inicio: `date`" >> reports/$file.txt
   		glpsol --model CFLP.mod --data dat/$file.dat --tmlim 7200 >> reports/$file.txt
	done
done