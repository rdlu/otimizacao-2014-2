# Implementacao de um simulated annealing genérico
class SimulateAnnealing
  def initialize(options = {})
    #recebe o objeto com o estado inicial
    #ele deve possuir os metodos total_cost, random_state e neighbour_state disponiveis para serem chamados
    @estado_inicial         = options[:estado_inicial]
    # chance pequenissima de reinicio usando o random_state
    @probabilidade_reinicio = options[:probabilidade_reinicio]  ? options[:probabilidade_reinicio]  : PROB_REINICIO_DEFAULT
    # um criterio extra de parada, numero de rodadas
    @rodadas_max            = options[:rodadas_max]             ? options[:rodadas_max]             : RODADAS_MAX_DEFAULT
    # outro criterio de parada: "energia" ou custo aceitaveis para a solucao 
    @energia_aceitavel      = options[:energia_aceitavel]       ? options[:energia_aceitavel]       : ENERGIA_MAX_DEFAULT
    # funcao que calcula a temperatura para a proxima rodada
    @temperature_fn         = options[:funcao_temperatura]      ? options[:funcao_temperatura]      : calcula_temp_futura(ALPHA_DEFAULT)
    # criterio extra de parada, tempo REAL (do relogio), em segundos, padrao de uma hora
    @hora_max               = options[:tempo_max]               ? Time.now + options[:tempo_max]    : Time.now + TEMPO_MAX_DEFAULT
    puts "Horario máximo para parada #{@hora_max} (#{options[:tempo_max].floor}) segundos"
  end
  
  # Calcula a a temperatura de acordo com a formula
  # T(k) = T0 * (alpha ** k)
  # Utilizada para o resfriamento
  # 'alpha' eh um numero multiplicador do resfriamento 0 < alpha < 1
  #        normalmente proximo de 1
  # retorna a temperatura 0 < temp < 1, para um dado tempo fornecido
  # Disciplina de redução conhecida como Simulated Quenching
  def calcula_temp_futura(alpha)
    lambda { |time| (alpha ** time) }
  end

  # Funcao de transicao
  # Essa eh a funcao utilizada caso nao seja fornecida outra
  # Assume 0 < 'temp' < 1
  # Retorno:
  # A probabidade que uma transicao ocorra (que a energia proposta na atual temperatura seja utilizada)
  # Baseada na probabilidade do Boltzmann Annealing
  def probabilidade_transicao(energia_do_estado_atual, energia_proposta, temp_atual)
    # energia melhor? CLARO que vamos fazer a transicao
    return 1 if energia_proposta < energia_do_estado_atual
    #caso contrario retorna uma determinada probabilidade, dependendendo de quao ruim eh
    Math.exp((energia_do_estado_atual-energia_proposta) * (1.0 - temp_atual))
  end

  # Roda a simulacao retornando o melhor estado que a simulacao encontrou antes de parar
  def start
    # Initial simulation setup
    estado_atual = @estado_inicial
    energia_atual = estado_atual.total_cost
    melhor_estado = estado_atual
    melhor_energia = energia_atual
    rodada_atual = 0 # tempo que reinicia com restarts
    rodada_cumulativa_atual = 0 # rodada cumulativa independente dos restarts

    while rodada_cumulativa_atual < @rodadas_max and melhor_energia > @energia_aceitavel and Time.now < @hora_max

      novo_estado = if rand < @probabilidade_reinicio
        rodada_atual = 0
        estado_atual.clone.random_state
      else
        estado_atual.clone.neighbour_state
      end

      nova_energia = novo_estado.calculate_costs
      tmp = @temperature_fn.call(rodada_atual)

      # Verificamos se transicionamos para o o novo estado
      if probabilidade_transicao(energia_atual, nova_energia, tmp) > rand
        estado_atual = novo_estado
        energia_atual = nova_energia
      end

      # Temos um bom candidato?
      if energia_atual < melhor_energia
        melhor_estado = estado_atual
        melhor_energia = energia_atual
        fabricas = estado_atual.open_factories
        puts "Melhor custo encontrado: #{melhor_estado.total_cost} (#{rodada_cumulativa_atual} / #{@rodadas_max}) [#{tmp}]"
        puts "Facilidades que foram abertas: #{fabricas.to_a.sort}"
      end

      rodada_atual += 1
      rodada_cumulativa_atual += 1

      if DEBUG
        puts "Rodada atual: #{rodada_cumulativa_atual}"
        puts "Energia atual: #{energia_atual}"
      end
    end

    return melhor_estado
  end
end



